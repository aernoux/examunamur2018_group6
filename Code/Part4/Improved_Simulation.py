from Code.Part2 import Customers as c
from copy import copy
from random import uniform
import pandas as pd
from numpy.random import choice


def simulation(TIME, consumption, drinks_prices, foods_prices, returning_customers, prob_returning=0.2,
               prob_tripadvisor=.1, budget_onetime=100, budget_tripadvisor=100, group_size_returning=1,
               group_size_onetime=1, group_size_tripavisor=1, group_prob_returning=[.0, 1.],
               group_prob_onetime=[.0, 1.], group_prob_tripadvisor=[.0, 1.]):
    """create the simulated data in regard of the probabilities and the number of the different custumers.
       Customers may not come, come alone or come in group
       It returns a DataFrame."""

    current_returning_customers = copy(returning_customers)  # Contains only returning customers who can come back

    row_list = list()  # Contains the simulated data
    N_CID = len(returning_customers) + 1

    # SImulation
    for index, time in TIME.items():
        # Determining the customer(s)
        customers = list()
        if uniform(0, 1) <= prob_returning:  # returning
            nbr_customers = choice(group_size_returning + 1, p=group_prob_returning)
            if len(current_returning_customers) < nbr_customers:
                continue

            for index_customer in choice(len(current_returning_customers), nbr_customers, replace=False):  # Faster to
                customers.append(current_returning_customers[index_customer])                              # use index

        else:                                                                                            # onetime
            if uniform(0, 1) <= prob_tripadvisor:  # TripAdvisorCustomer
                for i in range(choice(group_size_tripavisor+1, p=group_prob_tripadvisor)):
                    customers.append(c.TripAdvisorCustomer("CID" + str(N_CID), consumption, budget_tripadvisor))
                    N_CID += 1

            else:                                  # OnetimeCustomer
                for i in range(choice(group_size_onetime+1, p=group_prob_onetime)):
                    customers.append(c.OneTimeCustomer("CID" + str(N_CID), consumption, budget_onetime))
                    N_CID += 1

        # Ordering for every customer
        for index, customer in enumerate(customers):
            order = customer.order(str(time[11:-3]), drinks_prices, foods_prices)

            dict = {"TIME": str(time),
                    "CUSTOMER": customer.customerID,
                    "DRINKS": order[0],
                    "FOOD": order[1],
                    "PAYROLL AMOUNT": order[2]}

            row_list.append(dict)

            # Remove the returning customer if he cannot come back
            if isinstance(customer, c.ReturningCustomer) and not (customer.can_return(drinks_prices, foods_prices)):
                current_returning_customers.pop(current_returning_customers.index(customer))

    return pd.DataFrame(row_list, columns=["TIME", "CUSTOMER", "DRINKS", "FOOD", "PAYROLL AMOUNT"])
