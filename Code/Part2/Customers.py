from random import randint, uniform


class Customer:
    """This class defines Customer described by:
            - customerID
            - probabilities
            - budget
        A Customer can order some drinks and food
    """

    def __init__(self, customerID, probabilities, budget):  # constructor
        self.customerID = customerID
        self.probabilities = probabilities
        self.budget = budget

    def order(self, time, drinks_prices, foods_prices):
        """Method that simulates the order of a Customer"""
        proba_drinks = self.probabilities[time]["DRINKS"]  # Probability to order any drink at any time
        proba_foods = self.probabilities[time]["FOOD"]     # Probability to order any food at any time

        # Chose a drink that will be ordered
        random_value = uniform(0, .99999999)  # Not 1 because of the rounding of probabilities
        total = 0
        for keys, values in proba_drinks.items():
            total += values
            if random_value <= total:
                consumption_drink = keys
                break

        # Chose a food that will be ordered
        random_value = uniform(0, .99999999)  # Not 1 because of the rounding of probabilities
        total = 0
        for keys, values in proba_foods.items():
            total += values
            if random_value <= total:
                consumption_food = keys
                break

        # Order
        order_price = drinks_prices[consumption_drink] + foods_prices[consumption_food]
        self.budget -= order_price

        return consumption_drink, consumption_food, order_price


class OneTimeCustomer(Customer):
    """This subclass of Customer defines a customer that comes only one time to the coffee bar"""


class TripAdvisorCustomer(OneTimeCustomer):
    """This subclass of OneTimeCustomer defines a customer that comes only one time to the coffee bar
        and use TripAdvisor to find the coffee bar. In addition, he gives a tip"""

    def __init__(self, *args, **kwargs):  # Add an attribute (tip)
        OneTimeCustomer.__init__(self, *args, **kwargs)
        self.tip = randint(1, 10)

    def order(self, *args, **kwargs):  # Add the tip to the order
        order = Customer.order(self, *args, **kwargs)
        self.budget -= self.tip
        return order[0], order[1], order[2] + self.tip


class ReturningCustomer(Customer):
    """This subclass of Customer defines a customer that comes more than one time to the coffee bar.
    This customer has a history to keep a track of its purchases"""

    def __init__(self, *args, **kwargs):  # Add an attribute (history)
        Customer.__init__(self, *args, **kwargs)
        self.history = list()

    def order(self, *args, **kwargs):  # Add the order to the history
        order = Customer.order(self, *args, **kwargs)
        self.history.append({"DRINK": order[0], "FOOD": order[1], "AMOUNT": order[2], "BUDGET": self.budget})
        return order

    def can_return(self, drinks_prices, foods_prices):
        """Method that says if the customer has enough money to come back"""
        max_drink = 0  # Price of the most expensive drink
        for drink_price in drinks_prices.values():
            if max_drink < drink_price: max_drink = drink_price

        max_food = 0  # Price of the most expensive food
        for food_price in foods_prices.values():
            if max_food < food_price: max_food = food_price

        return self.budget >= max_drink + max_food


class HipsterCustomer(ReturningCustomer):
    """This sublcass of ReturningCustomer defines a customer that comes more than one time to the coffee bar
        and is a hipster"""